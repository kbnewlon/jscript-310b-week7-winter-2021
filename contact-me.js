// TODO
const formSelect = document.getElementById('reason');
const jobRelatedInputs = document.getElementsByClassName('job-related');
const codeRelatedInputs = document.getElementsByClassName('code-related');

// Display message in <small> element if input is invalid
const displayMessage = function(inputEl, message) {
  let errorEl = inputEl.parentElement.querySelector('.error');
  errorEl.innerText = message;
};

// Shows relevant inputs based on selected reason for contacting
formSelect.addEventListener('change', function(e) {
  if (this.value === 'job') {
    for (let i = 0; i < jobRelatedInputs.length; i++) {
      // remove hidden class
      jobRelatedInputs[i].classList.remove('d-none');
    }
    for (let i = 0; i < codeRelatedInputs.length; i++) {
      // add this class to hide fields
      codeRelatedInputs[i].classList.add('d-none');
    }
  } else {
    for (let i = 0; i < codeRelatedInputs.length; i++) {
      codeRelatedInputs[i].classList.remove('d-none');
    }
    for (let i = 0; i < jobRelatedInputs.length; i++) {
      jobRelatedInputs[i].classList.add('d-none');
    }
  }
});

const jobTitleInput = document.getElementById('job-title');
const websiteInput = document.getElementById('website');
const codeLangSelect = document.getElementById('code-language');

/**
 * Adds error message for select & fields controlled by select
 *
 * @returns {boolean} isValid
 */
const validateSelectFields = () => {
  if (formSelect.value === 'choose') {
    // invalid, must select one
    displayMessage(formSelect, 'Please select one');
    return false;
  }
  displayMessage(formSelect, '');

  if (formSelect.value === 'job') {
    let valid = true;
    // If not present, set a message
    if (jobTitleInput.value.length < 1) {
      displayMessage(jobTitleInput, 'Required');
      valid = false;
    } else {
      // Otherwise valid, clear error message
      displayMessage(jobTitleInput, '');
    }

    if (!/https?\:\/\/.+\..+/.test(websiteInput.value)) {
      displayMessage(websiteInput, 'Valid URL required');
      valid = false;
    } else {
      displayMessage(websiteInput, '');
    }
    return valid;
  }

  if (formSelect.value === 'code') {
    if (codeLangSelect.value === 'choose') {
      displayMessage(codeLangSelect, 'Please choose one');
      return false;
    }
    displayMessage(codeLangSelect, '');
  }
  return true;
};

const form = document.getElementById('contact-form');
const nameInput = document.getElementById('name');
const emailInput = document.getElementById('email');
const messageInput = document.getElementById('message');

// Validates inputs on submit
form.addEventListener('submit', function(e) {
  let hasError = false;
  if (nameInput.value.length < 3) {
    hasError = true;
    displayMessage(nameInput, 'Name must be 3 or more characters');
  } else {
    displayMessage(nameInput, '');
  }

  if (!/\w+@\w+\.\w+/.test(emailInput.value)) {
    hasError = true;
    displayMessage(emailInput, 'Email required, must be in valid format');
  } else {
    displayMessage(emailInput, '');
  }

  if (messageInput.value.length < 10) {
    hasError = true;
    displayMessage(messageInput, 'Message must be 10 or more characters');
  } else {
    displayMessage(messageInput, '');
  }

  hasError = !validateSelectFields() || hasError;

  if (hasError) {
    e.preventDefault();
  }
});
