// TODO
const selectEl = document.getElementById('contact-kind');

const setSelectValidity = function() {
  if (selectEl.value === 'choose') {
    selectEl.setCustomValidity('Must select an option');
    return;
  }
  selectEl.setCustomValidity('');
  const businessEl = document.querySelector('.business');
  const techEl = document.querySelector('.technical');

  if (selectEl.value === 'business') {
    businessEl.classList.remove('hide');
    techEl.classList.add('hide');
    businessEl.querySelector('input').required = true;
    techEl.querySelector('input').required = false;
  } else {
    businessEl.classList.add('hide');
    techEl.classList.remove('hide');
    businessEl.querySelector('input').required = false;
    techEl.querySelector('input').required = true;
  }
};

setSelectValidity();
selectEl.addEventListener('change', setSelectValidity);

const form = document.getElementById('connect-form');
form.addEventListener('submit', function(e) {
  const inputs = document.getElementsByClassName('validate-input');
  let formIsValid = true;

  Array.from(inputs).forEach(input => {
    const small = input.parentElement.querySelector('small');

    if (!input.checkValidity()) {
      formIsValid = false;
      small.innerText = input.validationMessage;
    } else {
      small.innerText = '';
    }
  });

  if (!formIsValid) {
    e.preventDefault();
  }
});
