/**
 * Car class
 * @constructor
 * @param {String} model
 */
class Car {
    constructor(model) {
      this.currentSpeed = 0;
      this.model = model;
    }
  
    accelerate() {
      this.currentSpeed++;
    }
  
    brake() {
      this.currentSpeed--;
    }
  
    toString() {
      return `${this.model} is currently going ${this.currentSpeed} MPH`;
    }
  }
  
  //  Create an instance, accelerate twice, brake once, and console log the instance.toString()
  const audi = new Car('TT');
  audi.accelerate();
  audi.accelerate();
  audi.brake();
  console.log(audi.toString());
  
  /**
   * ElectricCar class
   * @constructor
   * @param {String} model
   */
  class ElectricCar extends Car {
    constructor(model) {
      super(model);
      this.motor = 'electric';
    }
  
    accelerate() {
      super.accelerate();
      super.accelerate();
    }
  
    toString() {
      return `Electric ${this.model} is currently going ${this.currentSpeed} MPH`;
    }
  }
  
  //  Create an instance, accelerate twice, brake once, and console log the instance.toString()
  const tesla = new ElectricCar('Tesla');
  tesla.accelerate();
  tesla.accelerate();
  tesla.brake();
  console.log(tesla.toString());
  